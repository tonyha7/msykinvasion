import logging
import random
from itertools import islice
#网上抄的
logging.basicConfig(level=logging.DEBUG)
VALUES = '富强民主文明和谐自由平等公正法治爱国敬业诚信友善'

def str2utf8(Str):
    utfStr = ''.join([i.encode('utf-8').hex().upper() for i in Str])
    logging.debug('{} --> {}'.format(Str, utfStr))
    return utfStr


def utf82str(utfStr):
    Str = bytearray.fromhex(utfStr).decode('utf-8')
    logging.debug('{} --> {}'.format(utfStr, Str))
    return Str


def hex2duo(hexStr):
    duo = []
    for h in hexStr:
        numH = int(h, 16)
        if numH < 10:
            duo.append(numH)
        elif random.random() < 0.5:
            duo.append(10)
            duo.append(numH - 10)
        else:
            duo.append(11)
            duo.append(numH - 6)
    logging.debug('{} --> {}'.format(hexStr, duo))
    return duo


def duo2hex(duo):
    hexList = []
    if duo[-1] >= 10:
        duo = duo[:-1]
    lit = iter(enumerate(duo))
    for i, d in lit:
        if d < 10:
            hexList.append('{:X}'.format(d))
        elif d == 10:
            hexList.append('{:X}'.format(duo[i + 1] + 10))
            next(islice(lit, 1, 1), None)
        else:
            hexList.append('{:X}'.format(duo[i + 1] + 6))
            next(islice(lit, 1, 1), None)
    hexStr = ''.join(hexList)
    logging.debug('{} --> {}'.format(duo, hexStr))
    return hexStr


def duo2values(duo):
    value = ''.join([VALUES[2 * i] + VALUES[2 * i + 1] for i in duo])
    logging.debug('{} --> {}'.format(duo, value))
    return value


def values2duo(value):
    duo = []
    pureValue = [v for v in value if v in VALUES]
    for i, v in enumerate(pureValue[::2]):
        index = VALUES.index(v)
        if index % 2 == 0:
            duo.append(index // 2)
    logging.debug('{} --> {}'.format(value, duo))
    return duo


def valueEncode(s):
    return duo2values(hex2duo(str2utf8(s)))


def valueDecode(value):
    return utf82str(duo2hex(values2duo(value)))

def str2utf8(Str):
    for i in Str:
        for j in i.encode('utf-8').hex().upper():
            yield  j


def hex2duo(Str):
    for h in str2utf8(Str):
        numH = int(h, 16)
        if numH < 10:
            yield(numH)
        elif random.random() < 0.5:
            yield(10)
            yield(numH - 10)
        else:
            yield(11)
            yield(numH - 6)


def duo2values(Str):
    for i in hex2duo(Str):
        yield VALUES[2 * i] + VALUES[2 * i + 1]

VALUE_PAIR = ('富强', '民主', '文明', '和谐', '自由', '平等', "公正", '法治', "爱国", '敬业', '诚信', '友善')



def values2duo(value):
    it = iter(value)
    for v in it:
        spicemen = v + next(it)
        if spicemen in VALUE_PAIR:
            yield VALUE_PAIR.index(spicemen)


def duo2hex(value):
    it = iter(values2duo(value))
    for v in it:
        if v < 10:
            yield v
        elif v == 10:
            yield 10 + next(it)
        else:
            yield 6 + next(it)



def valueDecode(value):
    it = iter(duo2hex(value))
    for v in it:
        ca = (v<<4) + next(it)
        if ca  < 256 :
            yield bytes([ca])

    


print(b''.join(valueDecode('诚信自由公正敬业和谐爱国诚信和谐友善爱国自由友善平等友善法治诚信富强富强诚信自由平等诚信富强公正爱国爱国诚信自由法治敬业诚信富强爱国自由')).decode('utf-8'))

print("".join(duo2values("操你妈的")))
